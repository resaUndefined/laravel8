<ul class="navbar-nav bg-gradient-info sidebar sidebar-dark accordion" id="accordionSidebar">

    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Framework Laravel <sup>8</sup></div>
    </a>

    <hr class="sidebar-divider my-0">

    <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
        <a class="nav-link" href="/">
            <i class="fa fa-tachometer"></i>
            <span>Dashboard</span></a>
    </li>

    <hr class="sidebar-divider">

    @if(auth()->user()->role == 'admin')
        <div class="sidebar-heading">
            Master Data
        </div>

        <li class="nav-item {{ Request::is('users') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('users.index') }}">
                <i class="fa fa-users"></i>
                <span>USERS</span>
            </a>
        </li>   

        <hr class="sidebar-divider">

        <div class="sidebar-heading">
            Contoh sub menu 1
        </div>

        <li class="nav-item {{ Request::is('users') ? 'active' : '' }}">
            <a class="nav-link" href="#">
                <i class="fa fa-balance-scale"></i>
                <span>sub menu 1.1</span>
            </a>
        </li>

        <li class="nav-item {{ Request::is('users') ? 'active' : '' }}">
            <a class="nav-link" href="#">
                <i class="fa fa-balance-scale"></i>
                <span>sub menu 1.2</span>
            </a>
        </li>

        <hr class="sidebar-divider">

        <div class="sidebar-heading">
            Contoh sub menu 2
        </div>

        <li class="nav-item {{ Request::is('users') ? 'active' : '' }}">
            <a class="nav-link" href="#">
                <i class="fa fa-balance-scale"></i>
                <span>sub menu 2.1</span>
            </a>
        </li>
    @else
        {{-- khusus bukan admin --}}
    @endif

    <hr class="sidebar-divider d-none d-md-block">

    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
